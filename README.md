# README #

## Para qué es este repositorio? ##

* Este es un repositorio template con CD (Continuous Deployment) para Cloud Run en GPC 
* Cloud Run es un servicio administrado sin servidores para depliegue de Containers. 
* Caso de uso: La mayoria de los backends que se estan desplegando

### Configuración ###

* Dentro del en la seccion de "Repository settings" -> "PIPELINES" Es necesario tener configuradas las siguientes variables de entorno en Bitbucket.
* Recordar que la seccion "Repository variables" dentro de la configuracion del "PIPELINE" se divide en los distintos SCOPES. Por defecto utilizamos PRODUCCION y TEST 


Repository variables | Detalle
-------------------- | -------
PROJECT_NAME         | ID del proyecto en GCP
REGION_GCP           | Region donde se hara el deployment (us-central1 por defecto) 
KEY_FILE_RUN         | EL json en base64 del la services account con permisos para implementar (solicitar a Devops)


Deployments - ENVs | Detalle
------------------ | -------
CD_ALLOW           | Habilitado el despliegue continua, con [yes] esta autorizado, otro valor para inhabilitar
NV_TRAFFIC         | Seteo del trafico enviado para nuevas versiones que se implementen, opciones validas [0 a 100]. Por defecto utilizar 100
FREE_ACCESS        | Habilitacion del acceso libre al servicio, opciones validas con [yes] esta autorizado, otro valor para inhabilitar


### Instrucciones para el deploy ###

* Para la implementacion es necesario tener las varibles de entorno configuradas segun el item anterior. 
* Para las varliables de entorno se leeran las mismas de los archivos .env_develop , .env_staging y .env_production para los entornos de Test, Staging y Produccion respectivamente 
* Tener en cuenta que este pipeline deploya automaticamente para las ramas de master y develop

### Con quién hablo ante cualquier consulta? ###

* JCFavaro - Ntorcivia
* DevOps/Black team
